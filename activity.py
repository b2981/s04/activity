from numpy import *
rows = int(input("Enter number of rows: "))
columns = int(input("Enter number of columns: "))

matrixOne = zeros((rows,columns))
matrixTwo = zeros((rows,columns))
matrixSum = zeros((rows,columns))

print("Input the values for First matrix")
for i in range(rows):
	for k in range(columns):
		matrixOne[i][k] = input(f"matrix[{i}][{k}] = ")

print("Input the values for second matrix")
for i in range(rows):
	for k in range(columns):
		matrixTwo[i][k] = input(f"matrix[{i}][{k}] = ")

print(f'Display Matrix1 :\n {matrixOne}')
print(f'Display Matrix2 :\n {matrixTwo}')

for i in range(rows):
	for k in range(columns):
		matrixSum[i][k] = matrixOne[i][k] + matrixTwo[i][k]

print(f'Sum of two matrix :\n {matrixSum}')







